#include "mmapscreen.h"
#include "console.h"
using namespace lacus;
using namespace std;
#define MP mmapscreen
#define FPS(start) (CLOCKS_PER_SEC / (clock()-start))
MP::MP(int x,int y,int w,int h):
	initialized(0),
	x(x),y(y),w(w),h(h),
	display(XOpenDisplay(":0")),
	root(DefaultRootWindow(display)),
	screen(nullptr)
{
	memset(&window_attr,0,sizeof(window_attr));
	XGetWindowAttributes(display,root,&window_attr);
	screen=window_attr.screen;
	ximg=XShmCreateImage(display,DefaultVisualOfScreen(screen),DefaultDepthOfScreen(screen),ZPixmap,0,&shminfo,w,h);
	shminfo.shmid=shmget(IPC_PRIVATE,ximg->bytes_per_line*ximg->height,IPC_CREAT|0777);
	shminfo.shmaddr=ximg->data=(char*)shmat(shminfo.shmid,0,0);
	shminfo.readOnly=0;
	if(shminfo.shmid<0){
		fail("shit on shmid");
		terminate();
	};
	auto s1=XShmAttach(display,&shminfo);
	if(s1)
		info("shm attached ok");
	else{
		fail("shm attach failed");
		terminate();
	};
	//int ev_ret, ev_err;
	/*if (XFixesQueryExtension(display, &ev_ret, &ev_err)) {
		::XFixesSelectCursorInput(display,root,XFixesDisplayCursorNotifyMask);
	};*/
	initialized=1;
};
void *MP::get(){
	XShmGetImage(display,root,ximg,x,y,0x00FFFFFF);
	
	return ximg->data;
};
MP::~MP(){
	if(this->initialized)
		XDestroyImage(ximg);
	XShmDetach(display,&shminfo);
	shmdt(shminfo.shmaddr);
	XCloseDisplay(display);
};
