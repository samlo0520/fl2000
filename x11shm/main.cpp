#include <X11/Xlib.h>
#include <X11/Xutil.h>

#include <X11/extensions/XShm.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <iostream>
#include "console.h"
#include <string>
#include <cstdio>
#include <cstdlib>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdbool.h>
#include <termios.h>
#include <unistd.h>
#include <unistd.h>
#include "../include/fl2000_ioctl.h"
#include <thread>
#include <chrono>
#include "misc.h"
#include "mmapscreen.h"
using namespace std;
using namespace std::literals::chrono_literals;
using namespace lacus;



int main(int argc, const char **argv){
	if(argc<2){
		fail(string("no target device: usage: ") + argv[0] + " [device name]");
		return -1;
	};
	auto name=argv[1];
	int fd=open(name,O_RDWR);
	if(fd==-1) {
		fail("cannot open target: "+string(name));
		return fd;
	};
	auto ioctl_ret = ioctl(fd, IOCTL_FL2000_QUERY_MONITOR_INFO, &monitor_info);
	if (ioctl_ret < 0) {
		fprintf(stderr, "IOCTL_FL2000_QUERY_MONITOR_INFO fails %d\n",
			ioctl_ret);
		return -1;
	}
	mem_type=SURFACE_TYPE_VIRTUAL_FRAGMENTED_PERSISTENT;
	parse_edid();
	mmapscreen sc1;
	test_display_on_resolution(fd,1920,1080,sc1);

		return 0;
};
