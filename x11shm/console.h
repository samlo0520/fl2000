#ifndef __H_CONSOLE__
#define __H_CONSOLE__
#include <fstream>
#include <iostream>
namespace lacus{
	enum nfo_type{
		EE,
		II,
		WW
	};
	template<typename W> void echo(nfo_type type,const W& message){
		echo(type,message,std::cout);
	};
	template<typename W> void info(const W& message){
		echo(II,message);
	};
	template<typename W> void warn(const W& message){
		echo(WW,message);
	};
	template<typename W> void fail(const W& message){
		echo(EE,message);
	};
	template<typename S,typename W> void echo(nfo_type type,const W& message, S& stream){
		switch(type){
			case II:
				stream<<"\e[1;36m[I]\e[0m ";
				break;
			case EE:
				stream<<"\e[1;31m[E]\e[0m ";
				break;
			case WW:
				stream<<"\e[1;93m[W]\e[0m ";
		};
		stream<<message<<std::endl;
	};
};
#endif
