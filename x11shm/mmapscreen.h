#ifndef __H_MMAPSCREEN__
#define __H_MMAPSCREEN__
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/extensions/Xfixes.h>
#include <X11/extensions/XShm.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <iostream>
#include <cstring>
#include <memory>
struct mmapscreen{
	bool initialized;
	int x,y,w,h;
	Display *display;
	Window root;
	Screen *screen;
	XImage* ximg;
	XShmSegmentInfo shminfo;
	XWindowAttributes window_attr;
	mmapscreen(int x=1920,int y=0,int w=1920,int h=1080);
	void* get();
	~mmapscreen();
};
#endif
