
#include <opencv2/opencv.hpp>
#include "misc.h"
#include "mmapscreen.h"
struct monitor_info monitor_info;
struct resolution  resolution_list[64];
uint32_t num_resolution;
uint8_t frame_buffers[NUM_FRAME_BUFFERS][MAX_FRAME_BUFFER_SIZE];
uint32_t mem_type = 0;

struct test_alloc phy_frame_buffers[NUM_FRAME_BUFFERS];



/*
 * implementation
 */
int kbhit(void)
{
	struct termios oldt, newt;
	int ch;
	int oldf;

	tcgetattr(STDIN_FILENO, &oldt);
	newt = oldt;
	newt.c_lflag &= ~(ICANON | ECHO);
	tcsetattr(STDIN_FILENO, TCSANOW, &newt);
	oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
	fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);
	ch = getchar();
	tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
	fcntl(STDIN_FILENO, F_SETFL, oldf);
	if(ch != EOF) {
		ungetc(ch, stdin);
		return 1;
	}
	return 0;
}
int _alloc_frame(int fd, struct test_alloc * phy_alloc, uint32_t size)
{
	int ret;

	memset(phy_alloc, 0, sizeof(*phy_alloc));
	phy_alloc->buffer_size = (uint64_t) size;

	ret = ioctl(fd, IOCTL_FL2000_TEST_ALLOC_SURFACE, phy_alloc);
	if (ret < 0)
		fprintf(stderr,
			"%s: IOCTL_FL2000_TEST_ALLOC_SURFACE failed %d\n",
			__func__, ret);

	return ret;
}

void _release_frame(int fd, struct test_alloc * phy_alloc)
{
	int ret;

	ret = ioctl(fd, IOCTL_FL2000_TEST_RELEASE_SURFACE, phy_alloc);
	if (ret < 0)
		fprintf(stderr,
			"%s: IOCTL_FL2000_TEST_RELEASE_SURFACE failed %d\n",
			__func__, ret);
}


bool fl2000_is_connected(void)
{
	struct stat f_stat;
	int    ret_val;

	ret_val = stat(FL2K_NAME, &f_stat);

	if (ret_val == 0)
		return true;
	return false;
}
void add_resolution(uint32_t width, uint32_t height)
{
	unsigned int i;
	bool resolution_found;

	/*
	 * check if this resolution is already there
	 */
	resolution_found = false;
	for (i = 0; i < num_resolution; i++) {
		struct resolution * this1 = &resolution_list[i];

		if (this1->width == width && this1->height == height) {
			resolution_found = true;
			break;
		}
	}

	if (!resolution_found) {
		resolution_list[num_resolution].width = width;
		resolution_list[num_resolution].height = height;
		fprintf(stdout, "entry [%u] = (%u, %u) added\n",
			num_resolution, width, height);
		num_resolution++;
	}
};

void fl2000_monitor_ratio_to_dimension(
	uint8_t x,
	uint8_t aspect_ratio,
	uint32_t* width,
	uint32_t* height)
{
	uint32_t temp_width;
	uint32_t temp_height;

	temp_width = (x + 31) * 8;
	switch (aspect_ratio) {
	case IMAGE_ASPECT_RATIO_16_10:
		temp_height = (temp_width / 16) * 10;
		break;

	case IMAGE_ASPECT_RATIO_4_3:
		temp_height = (temp_width / 4) * 3;
		break;

	case IMAGE_ASPECT_RATIO_5_4:
	    temp_height = (temp_width / 5) * 4;
	    break;

	case IMAGE_ASPECT_RATIO_16_9:
	default:
	    temp_height = ( temp_width / 16 ) * 9;
	    break;

	}

	*width = temp_width;
	*height = temp_height;
}
void parse_edid(void)
{
	uint8_t i;
	uint32_t width;
	uint32_t height;
	uint8_t * const monitor_edid = monitor_info.edid;

	/*
	 * EDID offset 38 ~ 53. Standard timing information. Upto 8 2-bytes.
	 * Unused fields are filled with 01 01
	 */
	for (i = 38; i < 53; i+= 2) {
		uint8_t	 x = monitor_edid[i];
		uint8_t  ratio = monitor_edid[i + 1] >> 6;
		uint8_t  freq = (monitor_edid[i + 1] & 0x3F) + 60;

		if (monitor_edid[i] == 1 && monitor_edid[i + 1] == 1)
			break;

		fl2000_monitor_ratio_to_dimension(
			x,
			ratio,
			&width,
			&height);

		fprintf(stdout, "found (%u, %u) @ %u fps\n",
			width, height, freq);

		add_resolution(width, height);
	}

	/*
	 * check detailed timing descriptor
	 */
	for (i = 54; i < 125; i+= 18) {
		uint8_t *  entry = &monitor_edid[i];
		uint32_t h_active, v_active;

		/*
		 * NOT detailed timing descriptor
		 */
		if (entry[0] == 0 && entry[1] == 0)
			break;

		h_active = (entry[4] >> 4) << 8 | entry[2];
		v_active = (entry[7] >> 4) << 8 | entry[5];

		fprintf(stdout, "found (%u, %u) detailed timing desc\n",
			h_active, v_active);
		add_resolution(h_active, v_active);
	}
}



/*
 * return true if a test_%u_%u.bmp exist, and is 24bpp format.
 */
bool init_frame_by_bmp_file(
	uint8_t * frame_buffer,
	uint32_t width,
	uint32_t height,
	uint32_t index)
{
	uint32_t const frame_size = width * height * 3;
	char file_name[128];
	uint8_t header[54];
	size_t len;
	bool file_ok = false;
	FILE * bmp_file;
	uint32_t bmp_width;
	uint32_t bmp_height;
	uint32_t bmp_bpp;
	uint32_t i;

	memset(file_name, 0, sizeof(file_name));
	snprintf(file_name, sizeof(file_name), "test_%d_%d_%d.bmp",
		width, height, index);
	bmp_file = fopen(file_name, "r");
	if (bmp_file == NULL) {
		//fprintf(stderr, "%s not exists\n", file_name);
		goto exit;
	}

	/*
	 * read the header
	 */
	len = fread(header, 1, 54, bmp_file);
	if (len != 54) {
		fprintf(stderr, "%s len(%d) != 54 ?\n",
			file_name, (int) len);
		goto exit;
	}

	/*
	 * check header
	 */
	if (header[0] != 'B' || header[1] != 'M' || header[10] != 0x36 ||
	    header[14] != 0x28) {
		fprintf(stderr, "invalid header\n");
		goto exit;
	}

	bmp_width  = header[20] << 16 | header[19] << 8 | header[18];
	bmp_height = header[24] << 16 | header[23] << 8 | header[22];
	if (bmp_width != width || bmp_height != height) {
		fprintf(stderr, "bmp width/height (%u,%u) mismatch!\n",
			bmp_width, bmp_height);
		goto exit;
	}

	bmp_bpp = header[28];
	if (bmp_bpp != 24) {
		fprintf(stderr, "bmp_bpp(%d) not 24?\n", bmp_bpp);
		goto exit;
	}

	/*
	 * read the frame buffer from offset 54, in reverse order.
	 */
	for (i = 0; i < height; i++) {
		uint8_t * frame_offset;

		frame_offset = frame_buffer + (height - i - 1) * width * 3;
		fread(frame_offset, 1, width * 3,  bmp_file);
	}

	file_ok = true;

exit:
	if (bmp_file != NULL)
		fclose(bmp_file);
	return file_ok;
}

void test_display_on_resolution(int fd, uint32_t width, uint32_t height, mmapscreen& sc1)
{
	struct display_mode display_mode;
	struct surface_info surface_info;
	struct surface_update_info update_info;
	uint8_t * frame_buffer;
	int ret_val;
	int index;
	bool bmp_ok;
	unsigned int num_bmp;

	/*
	 * find how many bmp files are available, look for at most
	 * 16 test_xxxx_yyyy_z.bmp
	 */
	num_bmp = 0;
	for (index = 0; index < NUM_FRAME_BUFFERS; index++) {
		char file_name[128];
		FILE * bmp_file;

		memset(file_name, 0, sizeof(file_name));
		snprintf(file_name, sizeof(file_name), "test_%d_%d_%d.bmp",
			width, height, index);
		bmp_file = fopen(file_name, "r");
		if (bmp_file == NULL)
			break;
		fclose(bmp_file);
		num_bmp++;
	}


	/*
	 * set display mode
	 */
	memset(&display_mode, 0, sizeof(display_mode));
	display_mode.width = width;
	display_mode.height = height;
	display_mode.refresh_rate = 60;
	display_mode.input_color_format = COLOR_FORMAT_RGB_24;
	display_mode.output_color_format = COLOR_FORMAT_RGB_24;

	ret_val = ioctl(fd, IOCTL_FL2000_SET_DISPLAY_MODE, &display_mode);
	if (ret_val < 0) {
		fprintf(stderr, "IOCTL_FL2000_SET_DISPLAY_MODE failed %d\n",
			ret_val);
		goto exit;
	}


	/*
	 * create surfaces
	 */
	for (index = 0; index < num_bmp; index++) {
		memset(&surface_info, 0, sizeof(surface_info));

		switch (mem_type) {
		case SURFACE_TYPE_VIRTUAL_FRAGMENTED_VOLATILE:
		case SURFACE_TYPE_VIRTUAL_FRAGMENTED_PERSISTENT:
			frame_buffer = frame_buffers[index];
			init_frame_by_bmp_file(frame_buffer, width, height, index);
			surface_info.handle		= (unsigned long) frame_buffer;
			surface_info.user_buffer	= (unsigned long) frame_buffer;
			break;

		case SURFACE_TYPE_VIRTUAL_CONTIGUOUS:
			ret_val = _alloc_frame(fd, &phy_frame_buffers[index],
				width * height * 3);
			if (ret_val < 0)
				goto exit;

			fprintf(stderr,
				"usr_addr(0x%lx), phy_addr(0x%lx) allocated\n",
				(unsigned long) phy_frame_buffers[index].usr_addr,
				(unsigned long) phy_frame_buffers[index].phy_addr);
			frame_buffer = (uint8_t*) (unsigned long) phy_frame_buffers[index].usr_addr;
			init_frame_by_bmp_file(frame_buffer, width, height, index);
			surface_info.handle	 = phy_frame_buffers[index].usr_addr;
			surface_info.user_buffer = phy_frame_buffers[index].usr_addr;
			break;
		case SURFACE_TYPE_PHYSICAL_CONTIGUOUS:
			ret_val = _alloc_frame(fd, &phy_frame_buffers[index],
				width * height * 3);
			if (ret_val < 0)
				goto exit;

			fprintf(stderr,
				"usr_addr(0x%lx), phy_addr(0x%lx) allocated\n",
				(unsigned long) phy_frame_buffers[index].usr_addr,
				(unsigned long) phy_frame_buffers[index].phy_addr);
			frame_buffer = (uint8_t*) (unsigned long) phy_frame_buffers[index].usr_addr;
			init_frame_by_bmp_file(frame_buffer, width, height, index);
			surface_info.handle	 = phy_frame_buffers[index].usr_addr;
			surface_info.user_buffer = phy_frame_buffers[index].phy_addr;
			break;
		}

		surface_info.buffer_length	= width * height * 3;
		surface_info.width		= width;
		surface_info.height		= height;
		surface_info.pitch		= width * 3;
		surface_info.color_format	= COLOR_FORMAT_RGB_24;
		surface_info.type		= mem_type;
		ret_val = ioctl(fd, IOCTL_FL2000_CREATE_SURFACE, &surface_info);
		if (ret_val < 0) {
			fprintf(stderr, "IOCTL_FL2000_CREATE_SURFACE failed %d\n",
				ret_val);
			goto exit;
		}
	}

	/*
	 * for each primary surfaces, send update to kernel driver.
	 */
	index = 0;
	do {
		int c;

		if (!fl2000_is_connected())
			break;

		frame_buffer = frame_buffers[index];
		memset(&update_info, 0, sizeof(update_info));
		switch (mem_type) {
		case SURFACE_TYPE_VIRTUAL_FRAGMENTED_VOLATILE:
		case SURFACE_TYPE_VIRTUAL_FRAGMENTED_PERSISTENT:
			{
				using namespace std;
				using namespace cv;
				auto *stuff=sc1.get();
				auto *c_image=XFixesGetCursorImage(sc1.display);
				int root_x, root_y;
				int win_x, win_y;
				unsigned mask_return;
				Window window_returned;
				auto result=::XQueryPointer(sc1.display,sc1.root,&window_returned,&window_returned,
						&root_x,&root_y,&win_x,&win_y,&mask_return);
				Mat input(height,width,CV_8UC4,stuff),converted;
				cvtColor(input,converted,cv::COLOR_RGBA2RGB);
				if(root_x>=1920){
					int cw=1920-(root_x-1920-c_image->xhot),ch=1080-root_y+c_image->yhot;
					if(cw>24) cw=24;
					if(ch>24) ch=24;
					int wx=root_x-1920-c_image->xhot, wy=root_y-c_image->yhot;
					int ix=0,iy=0;
					if(wy<0){
						iy=wy;
						wy=0;
					};
					if(wx<0){
						ix=wx;
						wx=0;
					};
					Rect fffff(wx,wy,cw,ch);
					Mat subbar=converted(fffff),ci(24,24,CV_16UC4,c_image->pixels);
					for(int i=0-iy;i<ch;i++)
						for(int j=0-ix;j<cw;j++){
							auto &cpx=*reinterpret_cast<Vec4b*>(&ci.at<Vec4s>(i,j)[0]);
							auto &tpx=subbar.at<Vec3b>(i,j+ix);
							for(int k=0;k<3;k++)
								tpx[k]=tpx[k]*int(255-cpx[3])/255+int(cpx[3])*cpx[k]/255;
							
						};
				};
				::memcpy(frame_buffer,converted.data,height*width*3);
			};
			update_info.handle 	= (unsigned long) frame_buffer;
			update_info.user_buffer = (unsigned long) frame_buffer;
			break;

		case SURFACE_TYPE_VIRTUAL_CONTIGUOUS:
			update_info.handle	= phy_frame_buffers[index].usr_addr;
			update_info.user_buffer	= phy_frame_buffers[index].usr_addr;
			break;

		case SURFACE_TYPE_PHYSICAL_CONTIGUOUS:
			update_info.handle	= phy_frame_buffers[index].usr_addr;
			update_info.user_buffer	= phy_frame_buffers[index].phy_addr;
			break;
		default:
			fprintf(stderr, "unkown mem_type(%u)?\n", mem_type);
			break;
		}
		update_info.buffer_length 	= width * height * 3;

		ret_val = ioctl(fd, IOCTL_FL2000_NOTIFY_SURFACE_UPDATE, &update_info);
		if (ret_val < 0) {
			fprintf(stderr, "IOCTL_FL2000_NOTIFY_SURFACE_UPDATE failed %d\n",
				ret_val);
			goto exit;
		}

		if (1)
			index = 0;

		if (kbhit() == 0) {
			usleep(16666);	// sleep 60Hz
			continue;
		}

		c = getchar();
		if (c == 27)
			break;
	} while (true);

	/*
	 * disable output
	 */
	memset(&display_mode, 0, sizeof(display_mode));
	ret_val = ioctl(fd, IOCTL_FL2000_SET_DISPLAY_MODE, &display_mode);

	/*
	 * destroy all surfaces
	 */
	for (index = 0; index < num_bmp; index++) {
		frame_buffer = frame_buffers[index];
		memset(&surface_info, 0, sizeof(surface_info));
		switch (mem_type) {
		case SURFACE_TYPE_VIRTUAL_FRAGMENTED_VOLATILE:
		case SURFACE_TYPE_VIRTUAL_FRAGMENTED_PERSISTENT:
			surface_info.handle		= (unsigned long) frame_buffer;
			surface_info.user_buffer	= (unsigned long) frame_buffer;
			break;
		case SURFACE_TYPE_VIRTUAL_CONTIGUOUS:
			_release_frame(fd, &phy_frame_buffers[index]);
			surface_info.handle		= phy_frame_buffers[index].usr_addr;
			surface_info.user_buffer	= phy_frame_buffers[index].usr_addr;
			break;
		case SURFACE_TYPE_PHYSICAL_CONTIGUOUS:
			_release_frame(fd, &phy_frame_buffers[index]);
			surface_info.handle		= phy_frame_buffers[index].usr_addr;
			surface_info.user_buffer	= phy_frame_buffers[index].phy_addr;
			break;
		}
		surface_info.buffer_length	= width * height * 3;
		surface_info.width		= width;
		surface_info.height		= height;
		surface_info.pitch		= width * 3;
		surface_info.color_format	= COLOR_FORMAT_RGB_24;
		surface_info.type		= mem_type;
		ret_val = ioctl(fd, IOCTL_FL2000_DESTROY_SURFACE, &surface_info);
		if (ret_val < 0) {
			fprintf(stderr, "IOCTL_FL2000_DESTROY_SURFACE failed %d\n",
				ret_val);
			goto exit;
		}
	}

exit:;
}

