#ifndef __H_MISC__
#define __H_MISC__
#include <string>
#include <cstdio>
#include <cstdlib>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdbool.h>
#include <termios.h>
#include <unistd.h>
#include <unistd.h>
#include "mmapscreen.h"
#include "../include/fl2000_ioctl.h"
#define	FL2K_NAME	"/dev/fl2000-2"
#define IMAGE_ASPECT_RATIO_16_10                0
#define IMAGE_ASPECT_RATIO_4_3                  1
#define IMAGE_ASPECT_RATIO_5_4                  2
#define IMAGE_ASPECT_RATIO_16_9                 3

#define MAX_FRAME_BUFFER_SIZE			1920*1080*3
#define	NUM_FRAME_BUFFERS			1
/*
 * data structures
 */
struct resolution {
	uint32_t	width;
	uint32_t	height;
};

/*
 * global variables
 */
extern uint32_t mem_type;
extern struct monitor_info monitor_info;
void parse_edid();
void test_display_on_resolution(int fd, uint32_t width, uint32_t height, mmapscreen& screen_content);
#endif
